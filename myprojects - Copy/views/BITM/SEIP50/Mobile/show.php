<?php
include_once'../../../../vendor/autoload.php';

use App\Bitm\SEIP50\Mobile\Mobile;

$id = $_GET['id'];

$obj = new Mobile;

$oneData = $obj->show();
?>
<a href="index.php">Back</a>
<table border="1">
    <tr>        
        <th>ID</th>
        <th>Title</th>
        <th>Created</th>
        <th>Modified</th>
        <th>Action</th>
    </tr>


    <tr>                 
        <td><?php echo $oneData['id'] ?></td>
        <td><?php echo $oneData['title']; ?></td>
        <td><?php echo $oneData['created_at']; ?></td>
        <td><?php echo $oneData['modified_at']; ?></td>
        <td><a href="edit.php">Edit</a>|<a href="show.php?id=<?php echo $oneData['id']; ?>">Show</a>|
            <a href="trash.php">Trash</a>
        </td>

    </tr>

</table>
