<?php
session_start();
include_once'../../../../vendor/autoload.php';

use App\BITM\SEIP50\Mobile\Mobile;

$obj = new Mobile;
$allData = $obj->trashed();
//print_r($allData);


if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
?>

<center> <h3> Trashed Data</h3></center>

<center><table border="1">
    <tr>
        <th>SL No</th>
        <th>ID</th>
        <th>Title</th>
        <th>Action</th>
    </tr>

    <?php
    if (isset($allData) && !empty($allData)) {
        $i = '';
        foreach ($allData as $oneData) {
            $i++;
            ?>
            <tr>


                <td><?php echo $i; ?></td>
                <td><?php echo $oneData['id'] ?></td>
                <td><?php echo $oneData['title']; ?></td>
                <td><a href="recover.php?id=<?php echo $oneData['id']; ?>">Recover</a>|
                    <a href="delete.php?id=<?php echo $oneData['id']; ?>">Delete</a>
                </td>


            </tr>
    <?php }
} ?>
</table>
<a href="index.php">GO TO LIST</a><br/></center>