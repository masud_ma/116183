<?php
 include_once'../../../../vendor/autoload.php';
 use Basis\bitm\seip14\Mobile\Mobile;
 
 $mymobile = new Mobile();
 $myData = $mymobile->index();
?>
<html>
	<head>
		<title>Show All Data</title>
	</head>
	<body>
	<a href="create.php">Add New Models</a>
	
		<table border="1">
			<tr>
				<th>ID</th>
				<th>Mobile</th>
				<th>Created</th>
				<th>Action</th>
			</tr>
			<?php foreach($myData as $mobile){?>
			<tr>
				<td><?php echo $mobile['id']; ?></td>
				<td><?php echo $mobile['title']; ?></td>
				<td><?php echo $mobile['created_at']; ?></td>
				<td><a href="edit.php?id=<?php echo $mobile['id'];?>">Edit</a>|<a href="show.php?id=<?php echo $mobile['id']; ?>">Show</a>|<a href="delete.php?id=<?php echo $mobile['id']?>">Delete</a></td>
			</tr>
			<?php }?>
		</table>
		
	</body>
</html>