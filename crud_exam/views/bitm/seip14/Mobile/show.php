<?php 
include_once '../../../../vendor/autoload.php';
use Basis\bitm\seip14\Mobile\Mobile;
	$id = $_GET['id'];
	$mydataobj = new Mobile();
	$Onedata = $mydataobj->show($id);
?>
<html>
	<head>
		<title>Show All Data</title>
	</head>
	<body>
		<table border="1">
			<tr>
				<th>ID</th>
				<th>Mobile</th>
				<th>Created</th>
				<th>Action</th>
			</tr>
			
			<tr>
				<td><?php echo $Onedata['id']; ?></td>
				<td><?php echo $Onedata['title']; ?></td>
				<td><?php echo $Onedata['created_at']; ?></td>
				<td>
            <a href="edit.php">Edit</a> | 
            <a href="delete.php">Delete</a>
        </td>
			</tr>
		
		</table>
	</body>
</html>