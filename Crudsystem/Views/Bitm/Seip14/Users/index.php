<?php 
session_start();
include_once'../../../../vendor/autoload.php';
	use Basis\Bitm\Seip14\Users\Users;
	use Basis\Bitm\Seip14\Utility\Utility;
	
	
	
	$myobj = new Users;
	$allData = $myobj->index();
	
	
	$debugobj = new Utility;
	//$debugobj->debug($allData);
	//die;
	
	if(isset($_SESSION['alert'])){
		echo $_SESSION['alert'];
		unset($_SESSION['alert']);
	}
	
?>
<html>
	<head><title>Index Page</title></head>
	<body>
		<a href="create.php">Add a User</a><br/><br/>
		
		<table border="1">
			<tr>
				<th>SL</th>
				<th>Id</th>
				<th>Title</th>
				<th>Created</th>
				<th>Modified</th>
				<th>Action</th>
			</tr>
			<?php if(isset($allData) && !empty($allData)){
				$i=0;
			 foreach($allData as $myData){ 
				++$i;
				?>
			<tr>
				<td><?php echo $i; ?></td>
				<td><?php echo $myData['id']; ?></td>
				<td><?php echo $myData['title']; ?></td>
				<td><?php echo $myData['created']; ?></td>
				<td><?php echo $myData['modified']; ?></td>
				<td><a href="edit.php">Edit</a>|<a href="show.php?id=<?php echo $myData['id']; ?>">Show</a>|
				<a href="delete.php?id=<?php echo $myData['id']; ?>">Delete</a>|</td>
			</tr>
			<?php }}?>
		</table>
	</body>
</html>