<?php
namespace Basis\Bitm\Seip14\Birth;
class Birth{
    
    public $id = '';
    public $title = '';
    
    public function __construct() {
        $conn = mysql_connect('localhost','root','') or die('Unable To Connect');
        mysql_select_db('php14') or die('Unable To Connect With DB');
        //echo 'Hi';
    }
    public function store(){
        session_start();
        if(isset($this->title) &&!empty($this->title)){
            $query = "INSERT INTO `php14`.`birthday`(`title`)".
                    "VALUES('".$this->title."')";
            if(mysql_query($query)){
                $_SESSION['alert'] = 'You Have Successfully Submitted.'."<br/>";
            }
            else{
                 $_SESSION['alert'] = 'No Submitted Your Birthday.'."<br/>";
            }
            header('location:index.php');
        }
        else{
            
             $_SESSION['alert'] = 'No Submitted Your Birthday.'."<br/>";
             header('location:create.php');
        }
    }
    
    public function prepare($data=''){
        if(isset($data['title']) && !empty($data['title'])){
            $this->title = $data['title'];
        }
        else{
            echo 'No Submitted';
        }
        return $this;
    }
    
    public function index(){
        $allData = array();
        $query = "SELECT * FROM `php14`.`birthday` WHERE `deleted_at` IS NULL";
        $result = mysql_query($query);
        
        while($row = mysql_fetch_assoc($result)){
            $allData[] = $row;
        }
        return $allData;
    }
    
    public function show(){
        
        $this->id = $_GET['id'];
        
        $query = "SELECT * FROM `php14`.`birthday` WHERE id =".$this->id;
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        return $row;
    }
    
    public function Update($data=''){
        session_start();
        $this->id = $_GET['id'];
        $this->title = $data['title'];
        
        $query = "UPDATE `php14`.`birthday` SET `title` = '".$this->title."' WHERE id=".$this->id;
        if(mysql_query($query)){
            $_SESSION['alert'] = 'Successfully Updated'."<br/>";
            header('location:index.php');
        }
        else{
            $_SESSION['alert'] = 'No Successfully Updated'."<br/>";
            header('location:edit.php');
        }
    }
    
    public function trash(){
        session_start();
        $this->id= $_GET['id'];
        $this->deleted_at = time();
        
        $query = "UPDATE `php14`.`birthday` SET `deleted_at` = '".$this->deleted_at."' WHERE id=".$this->id;
        if(mysql_query($query)){
            $_SESSION['alert'] = 'Successfully Trashed'."<br/>";
            header('location:index.php');
        }
        else{
            $_SESSION['alert'] = 'No Successfully Trashed'."<br/>";
            header('location:index.php');
        }
    }
    
    public function trashed(){
       $allData = array();
        $query = "SELECT * FROM `php14`.`birthday` WHERE `deleted_at` IS NOT NULL";
        $result = mysql_query($query);
        
        while($row = mysql_fetch_assoc($result)){
            $allData[] = $row;
        }
        return $allData; 
    }
    
    public function recover(){
        session_start();
        $this->id= $_GET['id'];
        $query = "UPDATE `php14`.`birthday` SET `deleted_at`= NULL WHERE `id`=".$this->id;
        if(mysql_query($query)){
            $_SESSION['alert'] = 'Successfully Recovered'."<br/>";
            header('location:index.php');
        }
        else{
            $_SESSION['alert'] = 'No Successfully Recovered'."<br/>";
            header('location:trashed.php');
        }
    }
    public function delete(){
        $this->id = $_GET['id'];
        
        $query = "DELETE FROM `php14`.`birthday` WHERE `id` =".$this->id;
        if(mysql_query($query)){
            $_SESSION['alert'] = 'Successfully Deleted'."<br/>";
            header('location:trashed.php');
        }
        else{
            $_SESSION['alert'] = 'No Deleted'."<br/>";
            header('location:trashed.php');
        }
    }
    
}