<?php 
session_start();
if(isset($_SESSION['alert'])){
    echo $_SESSION['alert'];
    unset($_SESSION['alert']);
}

include_once'../../../../vendor/autoload.php';
use Basis\Bitm\Seip14\Mobile\Mobile;


$obj = new Mobile;
$mydata = $obj->index();
?>
<a href="create.php">Add Models</a>| <a href="trashed.php">Trashed Data</a>
<table border="1">
    <tr>
        <th>Sl No</th>
        <th>Id</th>
        <th>Title</th>
        <th>Action</th>
    </tr>
    <?php if(isset($mydata)&&!empty($mydata)){
        $i='';
        foreach ($mydata as $onedata){
            $i++;
        ?>
    <tr>
        <td><?php echo $i; ?></td>
        <td><?php echo $onedata['id'];?></td>
        <td><?php echo $onedata['title'];?></td>
        <td><a href="edit.php?id=<?php echo $onedata['id']; ?>">Edit</a>|
            <a href="show.php?id=<?php echo $onedata['id'];?>">Show</a>|
            <a href="trash.php?id=<?php echo $onedata['id']; ?>">Trash</a>
        </td>
    </tr>
    <?php }} ?>
</table>