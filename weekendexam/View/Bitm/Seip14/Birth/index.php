<?php
session_start();
include_once'../../../../vendor/autoload.php';
use Basis\Bitm\Seip14\Birth\Birth;


$obj = new Birth;
$alldata = $obj->index();
//print_r($alldata);

if(isset($_SESSION['alert'])){
    echo $_SESSION['alert'];
    unset($_SESSION['alert']);
}
?>
<html>
    <head><title>Show All Data</title></head>
 <body>
 <center>
    <a href="create.php">Add Birthday</a>| <a href="trashed.php">Trashed Data</a>
    <table border="1">
        <tr>
            <th>Sl No</th>
            <th>Id</th>
            <th>BirthDay</th>
            <th>Action</th>
        </tr>
        <?php 
        if(isset($alldata) && !empty($alldata)){
            $i='';
            foreach($alldata as $onedata){
                $i++;
        ?>
        <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $onedata['id']; ?></td>
            <td><?php echo $onedata['title']; ?></td>
            <td><a href="edit.php?id=<?php echo $onedata['id']; ?>">Edit</a>|
                <a href="show.php?id=<?php echo $onedata['id']; ?>">Show</a>|
                <a href="trash.php?id=<?php echo $onedata['id']; ?>">Trash</a>|
            </td>
            
            <?php }} else{ ?>

            <td colspan="4"> <?php echo 'No Data Avilabe';?></td>
            
            <?php }?>
        </tr>
        
    </table>
 </body></center>
</html>
