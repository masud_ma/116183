<?php
session_start();
include_once'../../../../vendor/autoload.php';
use Basis\Bitm\Seip14\Book\Book;


$obj = new Book;
$alldata = $obj->trashed();
//print_r($alldata);

if(isset($_SESSION['alert'])){
    echo $_SESSION['alert'];
    unset($_SESSION['alert']);
}
?>
<html>
    <head><title>Show All Data</title></head>
 <body>
 <center>
     <h3>Trashed Data</h3>
    <table border="1">
        <tr>
            <th>Sl No</th>
            <th>Id</th>
            <th>Books</th>
            <th>Action</th>
        </tr>
        <?php 
        if(isset($alldata) && !empty($alldata)){
            $i='';
            foreach($alldata as $onedata){
                $i++;
        ?>
        <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $onedata['id']; ?></td>
            <td><?php echo $onedata['title']; ?></td>
            <td><a href="recover.php?id=<?php echo $onedata['id']; ?>">Recover</a>|
               
                <a href="delete.php?id=<?php echo $onedata['id']; ?>">Delete</a>
            </td>
            
            <?php }} else{ ?>

            <td colspan="4"> <?php echo 'No Data Avilabe';?></td>
            <?php }?>
        </tr>
        
    </table>
     
 </body></center>
</html>
