<?php 
	include_once'../../../vendor/autoload.php';
	use Basis\bitm\Mobile\Mobile;
	
	$id = $_GET['id'];
	
	$obj = new Mobile();
	
	$oneData = $obj->show($id);
	
?>
<table border="1">
<tr>
	<th>ID</th>
	<th>Title</th>
	<th>Created</th>
	<th>Modified</th>
</tr>	
	<tr>
		<td><?php echo  $oneData['id']; ?></td>
		<td><?php echo $oneData['title']; ?></td>
		<td><?php echo $oneData['created_at']; ?></td>
		<td><?php echo $oneData['modified_at']; ?></td>
	</tr>
</table>