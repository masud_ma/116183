<?php 
	class Main{
		public $a = '';
		public $b = '';
				
		protected function sub($val1='', $val2=''){
			$this->a = $val1;
			$this->b = $val2;
			return $this->a - $this->b;
		}
		
		public function add(){
			echo "This is Public Method."."</br>";
			$obj = new Main;
			$result = $obj->sub(10,5);
			return $result;
		}
	}