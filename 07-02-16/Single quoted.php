<?php
echo 'this is a simple string';

echo 'You can also have embedded newlinews in strings this way as it is okay to do';


// Outputs: Arnold once said:  "I'll be back"
echo 'Aronold once said: "I\'ll be back"';

// Outputs: You deleted C:\*.*?
echo 'You deleted c:\\*.*?';

// Outputs: You deleted C:\*.*?
echo 'You deleted c:\*.*?';

// Outputs: Veriable do not $expand $either 
   echo 'Variable do not $expand $either';
?>