<?php

namespace App\BITM\SEIP50\Actors;

class Actors {

    public $id = '';
    public $actor = '';

    public function __construct() {
        $conn = mysql_connect('localhost', 'root', '') or die('Unable To Connect.');
        mysql_select_db('php14') or die('Unable To connect with DB');
        //echo 'hi';
    }

    public function prepare($data = array()) {
        if (isset($data['actor']) && !empty($data['actor'])) {
            $this->actor = $data['actor'];
        }
        if (isset($data['id']) && !empty($data['id'])) {
            $this->id = $data['id'];
        }
        return $this;
    }

    public function store() {
        session_start();
        if (isset($this->actor) && !empty($this->actor)) {
            $query = "INSERT INTO `php14`.`actors`(`actor`)" . "VALUES('" . $this->actor . "')";
            if (mysql_query($query)) {
                $_SESSION['alert'] = 'Successfully Submitted Data.' . "<br>";
            } else {
                $_SESSION['alert'] = 'No Successfully Submitted Data.';
            }
            header('location:index.php');
        } else {
            echo 'No Submiited Data.';
        }
    }

    public function index() {
        $allData = array();
        $query = "SELECT * FROM `actors` WHERE deleted_at IS NULL";
        $result = mysql_query($query);
        while ($row = mysql_fetch_assoc($result)) {
            $allData[] = $row;
        }
        return $allData;
    }

    public function show($id = '') {
        $this->id = $id;
        $query = "SELECT * FROM `actors` WHERE id=" . $this->id;
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        return $row;
    }

    public function Update() {
        session_start();
        $query = "UPDATE `php14`.`actors` SET `actor` ='" . $this->actor . "' WHERE id=" . $this->id;
        if (mysql_query($query)) {
            $_SESSION['alert'] = 'Successfully Updated Data.' . "<br>";
        } else {
            $_SESSION['alert'] = 'No Updated Data.';
        }
        header('location:index.php');
    }

    public function trash() {
        session_start();
        $this->id = $_GET['id'];
        $query = "UPDATE `actors` SET `deleted_at` = '" . date('Y: m: d') . "' WHERE id=" . $this->id;
        if (mysql_query($query)) {
            $_SESSION['alert'] = 'Successfully Trashed Data.' . "<br>";
        } else {
            $_SESSION['alert'] = 'No Trashed Data.';
        }
        header('location:index.php');
    }

    public function trashed() {
        $allData = array();
        $query = "SELECT * FROM `actors` WHERE deleted_at IS NOT NULL";
        $result = mysql_query($query);
        while ($row = mysql_fetch_assoc($result)) {
            $allData[] = $row;
        }
        return $allData;
    }

    public function recover() {
        session_start();
        $this->id = $_GET['id'];
        $query = "UPDATE `actors` SET `deleted_at` = NULL WHERE id=" . $this->id;
        if (mysql_query($query)) {
            $_SESSION['alert'] = 'Successfully Restore Data.' . "<br>";
        } else {
            $_SESSION['alert'] = 'No Restore Data.';
        }
        header('location:index.php');
    }

    public function delete() {
        session_start();
        $this->id = $_GET['id'];
        $query = "DELETE FROM `php14`.`actors` WHERE id=" . $this->id;
        if (mysql_query($query)) {
            $_SESSION['alert'] = 'Successfully Deleted Data.' . "<br>";
        } else {
            $_SESSION['alert'] = 'No Deleted Data.';
        }
        header('location:trashed.php');
    }

}
