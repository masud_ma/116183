<?php
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP50\Hobby\Hobbies;
use App\BITM\SEIP50\Utility\Utility;

$uti = new Utility();
$obj = new Hobbies();
$trashed = $obj->trashed();
//$uti->debug($trashed);

?>
<table border="1">
    <h1> Trashed Data</h1>
    <tr>
        <th>SL No:</th>
        <th>Id</th>
        <th>List of Hobby</th>
        <th>Action</th>
    </tr>
    <?php if(isset($trashed)&& !empty($trashed)){
        
        $i = '';
        foreach ($trashed as $onData){
            $i++;
    ?>
    <tr>
        <td><?php echo $i; ?>   </td>
        <td><?php echo $onData['id']; ?> </td>
        <td><?php echo $onData['hobby']; ?></td>
        <td><a href="recover.php?id=<?php echo $onData['id']; ?>">Restore</a>|
            <a href="show.php?id=<?php echo $onData['id']; ?>">Show</a>|
            <a href="delete.php?id=<?php echo $onData['id']; ?>">Delete</a>
        </td>
    </tr>
    <?php }}else {?>
    
    <tr>
        <td claspan="4"><?php echo 'No Data Avilabe'; ?></td>
    </tr>
    <?php  }?>
</table>
<a href="index.php">Go To Lists</a>