<?php
include_once'../../../../vendor/autoload.php';
use App\BITM\SEIP50\Hobby\Hobbies;
use App\BITM\SEIP50\Utility\Utility;

$uti = new Utility;
//$uti->debug($_POST);

$hobby = $_POST['hobby'];
//$uti->debug($hobby);

$comma_separeted = implode(",", $hobby);
//echo $comma_separeted;

$_POST['hobby'] = $comma_separeted;

//$uti->debug($_POST['hobby']);
$obj = new Hobbies;
$obj->prepare($_POST)->store();