<?php
session_start();
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP50\Hobby\Hobbies;
use App\BITM\SEIP50\Utility\Utility;

$id = $_GET['id'];

$uti = new Utility();
$obj = new Hobbies();
$trashed = $obj->recover($id);
//$uti->debug($trashed);
if(isset($_SESSION['alert'])){
    echo $_SESSION['alert'];
    unset($_SESSION['alert']);
}