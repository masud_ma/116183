<?php
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP50\Hobby\Hobbies;
use App\BITM\SEIP50\Utility\Utility;

$id = $_GET['id'];
$uti = new Utility();

$hobby = $_POST['hobby'];
$string = implode(',', $hobby);

$_POST['hobby'] = $string;

$obj = new Hobbies();
$obj->prepare($_POST)->Update($id);
