<?php
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP50\Hobby\Hobbies;
use App\BITM\SEIP50\Utility\Utility;

$id = $_GET['id'];
$uti = new Utility();

$obj = new Hobbies();
$singleData = $obj->show($id);

?>
<table border='1'>
    <h3> Your Hobby</h3>
    <tr>
        <th>Id</th>
        <th>Your Hobby</th>
        <th>Created</th>
        <th>Modified</th>
        <th>Action</th>
    </tr>
    <tr>
        <td><?php echo $singleData['id']; ?></td>
        <td><?php echo $singleData['hobby']; ?></td>
        
        <td><?php echo date('Y-M-D'); ?></td>
        <td> <?php echo date('Y-m-d | H: i: s'); ?></td>
        <td><a href="trash.php?id=<?php echo $singleData['id']; ?>">Trash</a></td>
    </tr>
</table>
<a href="index.php">Go To List</a>