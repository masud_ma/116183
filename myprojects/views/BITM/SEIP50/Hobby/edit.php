<?php
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP50\Hobby\Hobbies;
use App\BITM\SEIP50\Utility\Utility;

$id = $_GET['id'];
$uti = new Utility();

$obj = new Hobbies();
$singleData = $obj->show($id);

$all = $singleData['hobby'];
$item = explode(',', $all);
?>

<html>
<head>
    <title>Edit Page</title>
</head>
<body>
    <form  action="update.php?id=<?php echo $singleData['id']; ?>" method="POST">
        <div align="center"><br>
            <input type="checkbox" name="hobby[]" value="Milk" <?php if(in_array('Milk',$item)){echo 'checked';}else{echo '';} ?>> Milk<br>
            <input type="checkbox" name="hobby[]" value="Butter" <?php if(in_array('Butter',$item)){echo 'checked';}else{echo '';} ?>> Butter<br>
            <input type="checkbox" name="hobby[]" value="Cheese" <?php if(in_array('Cheese',$item)){echo 'checked';}else{echo '';} ?>> Cheese<br>
            <input type="submit" value="Save">
        <br>
        </div>
    </form>
</body>
</html>