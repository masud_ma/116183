<?php 
include_once'../../../../vendor/autoload.php';
use App\BITM\SEIP50\Gender\Gender;

$id = $_GET['id'];
$obj = new Gender;
$onData = $obj->show();
?>
<center>
    <h2>Your Single Data</h2>
    <table border="1">
        <tr>
            
            <th>Id</th>
            <th>Gender Title</th>
            <th>Created</th>
            <th>Modified</th>
            <th>Action</th>
        </tr>
    
        <tr>
           
            <td><?php echo $onData['id']; ?> </td>
            <td><?php echo $onData['title']; ?> </td>
            <td><?php echo $onData['created_at']; ?> </td>
            <td><?php echo $onData['modified_at']; ?> </td>
            <td><a href="edit.php?id=<?php echo $onData['id'];?>">Edit</a>|                
                <a href="trash.php?id=<?php echo $onData['id']; ?>">Trash</a>
            </td>       
        </tr>
        <a href="index.php">Back</a>
    </table>
    
</center>