<?php
include_once '../../../../vendor/autoload.php';

use App\BITM\SEIP50\Planguage\Planguage;
use App\BITM\SEIP50\Utility\Utility;

$uti = new Utility();


$single = $_POST['lang'];
//$uti->debug($single);
$all = implode(',', $single);
//$uti->debug($all);

$_POST['lang'] = $all;

$obj = new Planguage();
$obj->prepare($_POST)->Update();
