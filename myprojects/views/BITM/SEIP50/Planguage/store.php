<?php
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP50\Planguage\Planguage;
use App\BITM\SEIP50\Utility\Utility;

$uti = new Utility();

//$uti->debug($_POST);

$language = $_POST['lang'];

//$uti->debug($language);

$separet = implode(',', $language);
//echo $separet;
$_POST['lang'] = $separet;
$obj = new Planguage;
$obj->prepare($_POST)->store();
//$uti->debug($insert);