<?php
session_start();
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP50\Planguage\Planguage;
use App\BITM\SEIP50\Utility\Utility;

$uti = new Utility();
$obj = new Planguage();
$allData = $obj->index();
//$uti->debug($allData);

if(isset($_SESSION['alert'])){
   echo $_SESSION['alert'];
   unset($_SESSION['alert']);
}
?>
<html>
    <head><title>All Data</title></head>
    <body>
        <a href="create.php">Select Your Language</a>|<a href="trashed.php">Trashed Data</a>
        <table border="1">
            <tr>
                <th>SL No</th>
                <th>Id</th>
                <th>Programming Language List</th>
                <th>Action</th>
            </tr>
            
            <?php if(isset($allData) && !empty($allData)){
                $i= '';
                foreach ($allData as $onData){
                    $i++;
             ?>
            <tr>
                <td><?php echo $i++; ?></td>
                <td><?php echo $onData['id']; ?></td>
                <td><?php echo $onData['title']; ?></td>
                <td><a href="edit.php?id=<?php echo $onData['id']; ?>">Edit</a>|
                    <a href="show.php?id=<?php echo $onData['id']; ?>">Show</a>|
                    <a href="trash.php?id=<?php echo $onData['id']; ?>">Trash</a>
                </td>
                
            </tr>
            <?php }} else {  ?>
            <tr>
                <td><?php echo 'No Data Avilable.'?></td>
            </tr>
            <?php } ?>
        </table>
    </body>
</html>
