<?php
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP50\Planguage\Planguage;
use App\BITM\SEIP50\Utility\Utility;

$id = $_GET['id'];


$uti = new Utility();
$obj = new Planguage();
$myData = $obj->show();
//$uti->debug($myData);
?>
<html>
    <head><title>Your Data</title></head>
    <body>
        <a href="index.php">Go To List</a>
        <table border="1">
            <tr>
                
                <th>Id</th>
                <th>Programming Language List</th>
                <th>Created</th>
                <th>Modified</th>
                <th>Action</th>
            </tr>
             
            <tr>
                
                <td><?php echo $myData['id']; ?></td>
                <td><?php echo $myData['title']; ?></td>
                 <td><?php echo date('D:m:d'); ?></td>
                  <td><?php echo date('D:m:d'); ?></td>
                <td><a href="edit.php?id=<?php echo $myData['id']; ?>">Edit</a>|
                    
                    <a href="trash.php?id=<?php echo $myData['id']; ?>">Trash</a>
                </td>
                
            </tr>

            
        </table>
    </body>
</html>