<?php
session_start();
if(isset($_SESSION['alert'])){
    echo $_SESSION['alert'];
    unset($_SESSION['alert']);
}
include_once'../../../../vendor/autoload.php';
use App\BITM\SEIP50\Birth\Birth;

$obj = new Birth;
$myData = $obj->index();
//print_r($myData);

?>

<center>
    <a href="create.php">Add Birthday</a>|<a href="trashed.php">Trashed</a>
    <table border="1">
        <tr>
            <th>Sl No</th>
            <th>Id</th>
            <th>Birth Title</th>
            <th>Action</th>
        </tr>
        <?php if(isset($myData)&& !empty($myData)){
            $i='';
            foreach ($myData as $onData){
              $i++;  
        ?>
        <tr>
            <td><?php echo $i; ?> </td>
            <td><?php echo $onData['id']; ?> </td>
            <td><?php echo $onData['title']; ?> </td>
            <td><a href="edit.php?id=<?php echo $onData['id'];?>">Edit</a>|
                <a href="show.php?id=<?php echo $onData['id'];  ?>">Show</a>|
                <a href="trash.php?id=<?php echo $onData['id']; ?>">Trash</a>
            </td>
        </tr>
        <?php }}
        
        else{
            echo "No Data Available.";
        }
        ?>
    </table>
    
</center>