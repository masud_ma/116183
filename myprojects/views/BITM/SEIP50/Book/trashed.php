<?php
session_start();
if(isset($_SESSION['alert'])){
    echo $_SESSION['alert'];
    unset($_SESSION['alert']);
}
include_once'../../../../vendor/autoload.php';
use App\BITM\SEIP50\Book\Book;

$obj = new Book;
$myData = $obj->trashed();
//print_r($myData);

?>

<center>
    <a href="create.php">Add Book Title</a>
    <table border="1">
        <tr>
            <th>Sl No</th>
            <th>Id</th>
            <th>Book Title</th>
            <th>Action</th>
        </tr>
        <?php if(isset($myData)&& !empty($myData)){
            $i='';
            foreach ($myData as $onData){
              $i++;  
        ?>
        <tr>
            <td><?php echo $i; ?> </td>
            <td><?php echo $onData['id']; ?> </td>
            <td><?php echo $onData['title']; ?> </td>
            <td><a href="recover.php?id=<?php echo $onData['id'];?>">Recover</a>|
                <a href="show.php?id=<?php echo $onData['id'];  ?>">Show</a>|
                <a href="delete.php?id=<?php echo $onData['id']; ?>">Delete</a>
            </td>
        </tr>
        <?php }}
        
        else{
            echo "No Data Available.";
        }
        ?>
    </table>
    
</center>