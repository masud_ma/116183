<?php
session_start();
include_once '../../../../vendor/autoload.php';

use App\BITM\SEIP50\Actors\Actors;
use App\BITM\SEIP50\Utility\Utility;

$uti = new Utility();
$obj = new Actors;

$allData = $obj->index();
//$uti->debug($allData);


if (isset($_SESSION['alert'])) {
    echo $_SESSION['alert'];
    unset($_SESSION['alert']);
}
?>
<a href="create.php">Add Your Favorite Actor</a>|<a href="trashed.php">Trashed Data</a>
<table border="1">
    <tr>
        <th>SL NO</th>
        <th>Id</th>
        <th>Actors List</th>
        <th>Action</th>
    </tr>
    <?php
    if (isset($allData) && !empty($allData)) {
        $i = '';
        foreach ($allData as $onData) {

            $i++;
            ?>
            <tr>
                <td> <?php echo $i; ?></td>
                <td><?php echo $onData['id']; ?></td>
                <td><?php echo $onData['actor']; ?></td>
                <td><a href="edit.php?id=<?php echo $onData['id']; ?>">Edit</a>|
                    <a href="show.php?id=<?php echo $onData['id']; ?>">Show</a>|
                    <a href="trash.php?id=<?php echo $onData['id']; ?>">Trash</a>
                </td>
            </tr>
    <?php }
} else { ?>
        <tr>
            <td><?php echo 'No Data Available.'; ?></td>
        </tr>
<?php } ?>
</table>