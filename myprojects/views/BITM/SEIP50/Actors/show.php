<?php
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP50\Actors\Actors;
use App\BITM\SEIP50\Utility\Utility;

$id = $_GET['id'];

$uti = new Utility();
$obj = new Actors;

$allData = $obj->show($id);
//$uti->debug($allData);
?>
<table border="1">
    <tr>
       
        <th>Id</th>
        <th>Actors List</th>
         <th>Created</th>
          <th>Modified</th>
        <th>Action</th>
    </tr>
    
    <tr>
        
        <td><?php echo $allData['id']; ?></td>
        <td><?php echo $allData['actor']; ?></td>
        <td><?php echo date('Y: m: d') ?></td>
        <td><?php echo date('Y: m: d') ?></td>
        <td><a href="edit.php?id=<?php echo $allData['id']; ?>">Edit</a>|
        
        <a href="trash.php?id=<?php echo $allData['id']; ?>">Trash</a>
        </td>
    </tr>
</table>
