<?php
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP50\Actors\Actors;
use App\BITM\SEIP50\Utility\Utility;

$id = $_GET['id'];

$uti = new Utility();
$obj = new Actors;

$allData = $obj->show($id);
//$uti->debug($allData);

$all = $allData['actor'];
$itm = explode(',', $all);

//$uti->debug($itm);

?>
<!DOCTYPE html>
<html>
<body>

<form action="update.php" method="POST">
    <input type="checkbox" name="actor[]" value="Sumon Mahmud" <?php if(in_array('Sumon Mahmud',$itm)){echo 'checked';}else{ echo '';} ?>>Sumon Mahmud<br>
    <input type="checkbox" name="actor[]" value="Eamin Hosen" <?php if(in_array('Eamin Hosen',$itm)){echo 'checked';}else{ echo '';} ?>>Eamin Hosen<br>
    <input type="checkbox" name="actor[]" value="Ekram Khan" <?php if(in_array('Ekram Khan',$itm)){echo 'checked';}else{ echo '';} ?>>Ekram Khan<br>
    <input type="checkbox" name="actor[]" value="Abu Taleb"  <?php if(in_array('Abu Taleb',$itm)){echo 'checked';}else{ echo '';} ?>>Abu Taleb<br>
    <input type="hidden" name="id" value="<?php echo $id; ?>"/>
    <input type="submit" value="Save"/>
</form>

</body>
</html>

