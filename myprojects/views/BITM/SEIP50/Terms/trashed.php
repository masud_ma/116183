<?php 
session_start();
include '../../../../vendor/autoload.php';
use App\BITM\SEIP50\Terms\Terms;

$obj = new Terms();
$myData = $obj->trashed();
//print_r($myData);
if(isset($_SESSION['alert'])){
    echo $_SESSION['alert'];
    unset($_SESSION['alert']);
}

?>

<center>
    <h2>Trashed Data</h2>
    <table border="1">
        <tr>
            <th>Sl No</th>
            <th>Id</th>
            <th>Name</th>
            <th>Checkbox</th>
            <th>Action</th>
        </tr>
        <?php if(isset($myData)&& !empty($myData)){
            $i='';
            foreach ($myData as $onData){
              $i++;  
        ?>
        <tr>
            <td><?php echo $i; ?> </td>
            <td><?php echo $onData['id']; ?> </td>
            <td><?php echo $onData['title']; ?> </td>
            <td><?php echo $onData['checkbox']; ?> </td>
            <td><a href="recover.php?id=<?php echo $onData['id'];?>">Recover</a>|
                <a href="show.php?id=<?php echo $onData['id'];  ?>">Show</a>|
                <a href="delete.php?id=<?php echo $onData['id']; ?>">Delete</a>
            </td>
        <?php }} else{
            ?>
            <td colspan="4"><?php echo 'No Data Availabe'; ?></td>
      <?php   }?>
        </tr>
        
    </table>
    <a href="index.php">Back</a>
    
</center>


