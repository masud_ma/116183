<?php
session_start();

if(isset($_SESSION['alert'])){
    echo $_SESSION['alert'];
    unset($_SESSION['alert']);
}

include_once'../../../../vendor/autoload.php';
use App\BITM\SEIP50\Terms\Terms;

$obj = new Terms;
$myData = $obj->index();
?>

<center>
    <a href="create.php">Terms & Condition </a>|<a href="trashed.php">Trashed</a>
    <table border="1">
        <tr>
            <th>Sl No</th>
            <th>Id</th>
            <th>Name</th>
            <th>Checkbox</th>
            <th>Action</th>
        </tr>
        <?php if(isset($myData)&& !empty($myData)){
            $i='';
            foreach ($myData as $onData){
              $i++;  
        ?>
        <tr>
            <td><?php echo $i; ?> </td>
            <td><?php echo $onData['id']; ?> </td>
            <td><?php echo $onData['title']; ?> </td>
            <td><?php echo $onData['checkbox']; ?> </td>
            <td><a href="edit.php?id=<?php echo $onData['id'];?>">Edit</a>|
                <a href="show.php?id=<?php echo $onData['id'];  ?>">Show</a>|
                <a href="trash.php?id=<?php echo $onData['id']; ?>">Trash</a>
            </td>
        <?php }} else{
            ?>
            <td colspan="4"><?php echo 'No Data Availabe'; ?></td>
      <?php   }?>
        </tr>
        
    </table>
    
</center>


